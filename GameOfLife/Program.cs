﻿using System.Threading;

namespace GameOfLife
{
    public static class Program
    {
        // Constants for the game rules.
        private const int Height = 30, Width = 30, MaxRuns = 100;

        public static void Main(string[] args)
        {
            var runs = 0;

            var sim = new LifeSimulation(Height, Width);

            while (runs++ < MaxRuns)
            {
                sim.DrawAndGrow();

                // Give the user a chance to view the game in a more reasonable speed.
                Thread.Sleep(1000);
            }
        }
    }
}
