﻿using System;

namespace GameOfLife
{
    /// <summary>
    /// Conway's Game of Life in C# (Console App) with basic arrays and without any fancy complicated stuff.
    /// No error checking included.
    /// </summary>
    public class LifeSimulation
    {
        private readonly int _height;

        private readonly int _width;

        private readonly bool[,] _cells;

        /// <summary>
        /// Initializes a new Game of Life.
        /// </summary>
        /// <param name="height">Height of the cell field.</param>
        /// <param name="width">Width of the cell field.</param>
        public LifeSimulation(int height, int width)
        {
            _height = height;

            _width = width;

            _cells = new bool[height, width];

            GenerateField();
        }

        /// <summary>
        /// Advances the game by one generation and prints the current state to console.
        /// </summary>
        public void DrawAndGrow()
        {
            DrawGame();

            Grow();
        }

        /// <summary>
        /// Advances the game by one generation according to GoL's rule-set.
        /// </summary>
        private void Grow()
        {
            for (var i = 0; i < _height; i++)
            {
                for (var j = 0; j < _width; j++)
                {
                    var numberOfAliveNeighbors = GetNeighbors(i, j);

                    if (_cells[i, j])
                    {
                        if (numberOfAliveNeighbors < 2) _cells[i, j] = false;

                        if (numberOfAliveNeighbors > 3) _cells[i, j] = false;
                    }
                    else
                    {
                        if (numberOfAliveNeighbors == 3) _cells[i, j] = true;
                    }
                }
            }
        }

        /// <summary>
        /// Checks how many alive neighbors are in the vicinity of a cell.
        /// </summary>
        /// <param name="x">X-coordinate of the cell.</param>
        /// <param name="y">Y-coordinate of the cell.</param>
        /// <returns>The number of alive neighbors.</returns>
        private int GetNeighbors(in int x, in int y)
        {
            var numberOfAliveNeighbors = 0;

            for (var i = x - 1; i < x + 2; i++)
            {
                for (var j = y - 1; j < y + 2; j++)
                {
                    if ((i < 0 || j < 0) || (i >= _height || j >= _width)) continue;
                    if (_cells[i, j]) numberOfAliveNeighbors++;
                }
            }

            return numberOfAliveNeighbors;
        }

        /// <summary>
        /// Draws the game to the console.
        /// </summary>
        private void DrawGame()
        {
            for (var i = 0; i < _height; i++)
            {
                for (var j = 0; j < _width; j++)
                {
                    Console.Write(_cells[i, j] ? "x " : ". ");
                    if (j == _width - 1) Console.WriteLine("\r");
                }
            }

            Console.SetCursorPosition(0, Console.WindowTop);
        }

        /// <summary>
        /// Initializes the field with random boolean values.
        /// </summary>
        private void GenerateField()
        {
            var generator = new Random();

            for (var i = 0; i < _height; i++)
            {
                for (var j = 0; j < _width; j++)
                {
                    var number = generator.Next(2);
                    _cells[i, j] = number != 0;
                }
            }
        }
    }
}